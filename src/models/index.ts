
import { UserService } from "./users";

export const models = (): Datasources => {
  return {
    Users: new UserService()
  }
}

export interface Datasources {
  Users: UserService
}
