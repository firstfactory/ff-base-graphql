import UserDBInstance, { User, UserDB, } from "../datasources/usersDB";

export class UserService {
  private db: UserDB;
  constructor() {
    this.db = UserDBInstance
  }
  public getAll() {
    return this.db.all();
  }
  public addUser(user: User) {
    return this.db.add(user);
  }
}
