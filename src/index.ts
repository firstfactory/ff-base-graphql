import { GraphQLServer } from 'graphql-yoga'
import { resolvers } from './resolvers/resolvers';
import * as dotenv from "dotenv";
import { models } from './models';
dotenv.config();

const server = new GraphQLServer({
  typeDefs: './src/schema/schema.graphql',
  resolvers,
  context: req => ({
    ...req,
    models: models()
  }),
})
server.start({ port: Number(process.env.YOGA_PORT), }, () => console.log(`Server is  up and running on ${process.env.YOGA_ENDPOINT}`))
