import { Datasources } from "../models";

export interface Context {
    request: any
    models: Datasources
}
