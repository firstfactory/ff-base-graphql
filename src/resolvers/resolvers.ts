import { users } from "./userData/users.query";
import { addUser } from "./userData/addUser.mutation";


export const resolvers = {
    Query: {
        users
    },
    Mutation: {
        addUser
    }
}