import { Context } from "../../types/context";
import { GraphQLResolveInfo } from "graphql";
import { User } from "../../datasources/usersDB";

export const users = (parent, args, context: Context, info: GraphQLResolveInfo): User[] => {
  const { Users } = context.models;
  return Users.getAll()
}