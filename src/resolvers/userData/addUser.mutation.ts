import { User } from "../../datasources/usersDB";
import { Context } from "../../types/context";

export const addUser = (parent, args: { data: User }, context: Context, info) => {
  const { Users } = context.models;
  return Users.addUser(args.data)
}